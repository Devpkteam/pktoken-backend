var express = require('express');
//var socket = io.connect('http://localhost:3000', {'forceNew': true});
const multer = require('multer');
var app = express();
var bcrypt = require('bcryptjs');
var Inventario = require('../models/inventario');
var mdAutenticacion = require('../middlewares/autenticacion');
var gcm = require("node-gcm");
var Objectid = require('mongodb').ObjectID;
var sender = new gcm.Sender('AAAAHm1JpCw:APA91bFkZ8juWGdArZ5SKM0KqKwZ777MPa-ANZNAnAXsFwX4MDM5QbiJkLK7lYn81VSf_ADdOfEWfD1gtmGgExui-OCt2bdGbjUrWKdHK9qeYaHnwIjGni-4DmPDt1TvK_1Ibp3xQG1k');
// Obtener todos los productos.

app.get('/', ( req, res, next ) => {

    var desde = req.query.desde || 0;
    desde = Number(desde);

    Inventario.find({  }, 'nombre cantidad costo costo_inversion') // se selecciona de las colecciones los campos menos el de la pwd.
    .skip(desde)
    .exec(
        ( err, inventario ) => {
            if( err ) {
                return res.status(200).json({
                    ok: false,
                    msj: 'Error cargando productos',
                    errors: err
                });
            } 

        Inventario.count({}, (err, conteo) =>{
            res.status(200).json({
                ok: true,
                inventario: inventario,
                total: conteo
            });
        });
    });
});

app.get ('/:id', (req,res) =>{
    var id = req.params.id;
    Inventario.find({ _id:Objectid(id) })  
    .exec((err,inventario)=>{
        if (err) {
            return res.status(200).json({
                ok: false,
                msj: 'Error cargando producto',
                errors: err
            });
        }

        res.status(200).json({
            ok:true,
            inventario:inventario
        })
    }) 
});

// Crear nuevo producto

app.post('/' ,( req, res ) => {
    var body = req.body;
    var inventario = new Inventario({
        nombre: body.nombre,
        cantidad: body.cantidad,
        costo: body.costo,
        costo_inversion: body.costo_inversion
    });

    

    inventario.save( ( err, inventarioGuardado ) => {

        if( err ) {
            return res.status(500).json({
                ok: false,
                msj: 'Error al buscar producto',
                errors: err
            });
        }

        res.status(201).json({
            ok: true,
            inventario: inventarioGuardado,
            inventarioToken: req.inventario
        });
    });
})

app.post('/tokenfcm/:id',(req,res)=>{
    let body = req.body;
    let id = req.params.id

    Inventario.findByIdAndUpdate(id,{tokenFCM:body.token},(err,inventario)=>{
        console.log(inventario)
    })
})

//Actualizar inventario

app.put('/:id', (req, res) => {
    var id = req.params.id;
    var body = req.body;
console.log(body.nombre)
    Inventario.findById(id, (err, inventario) => {
        if( err ) {
            return res.status(500).json({
                ok: false,
                msj: 'Error al buscar producto',
                errors: err
            });
        }
    
        if (!inventario) {
            return res.status(400).json({
                ok: false,
                msj: 'El producto con el id '+ id + ' no existe',
                errors: { message: 'No existe un producto con ese ID' }
            });
        }
        inventario.nombre = body.nombre;
        inventario.cantidad = body.cantidad;
        inventario.costo = body.costo;
        inventario.costo_inversion = body.costo_inversion;
        
        inventario.save( (err, inventarioGuardado) => {
            if ( err ) {
                return res.status(400).json({
                    ok: false,
                    msj: 'Error al actualizar producto',
                    errors: err
                });
            }

            res.status(200).json({
                ok: true,
                inventario: inventarioGuardado
            });

        })

    });
});

// Borrar producto

app.delete('/:id',( req, res ) => {
    var id = req.params.id;

    Inventario.findByIdAndRemove(id, (err, inventarioBorrado) => {
        if( err ) {
            return res.status(500).json({
                ok: false,
                msj: 'Error al borrar producto',
                errors: err
            });
        }

        if( !inventarioBorrado ) {
            return res.status(500).json({
                ok: false,
                msj: 'No existe producto con ese id',
                errors: { message: 'No existe producto con ese id'}
            });
        }

        res.status(200).json({
            ok: true,
            inventario: inventarioBorrado
        });
    })
});




module.exports = app;