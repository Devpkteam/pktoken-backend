var express = require('express');
let pktoken = require('../models/pktoken');
var Usuario = require('../models/usuario');
var Objectid = require('mongodb').ObjectID;
var Pksolicitudes = require('../models/solicitudes');
var app = express();

var gcm = require("node-gcm");
var sender = new gcm.Sender('AAAAHm1JpCw:APA91bFkZ8juWGdArZ5SKM0KqKwZ777MPa-ANZNAnAXsFwX4MDM5QbiJkLK7lYn81VSf_ADdOfEWfD1gtmGgExui-OCt2bdGbjUrWKdHK9qeYaHnwIjGni-4DmPDt1TvK_1Ibp3xQG1k');



app.get('/', (req,res)=>{
    Pksolicitudes.find({})
        .populate('solicitante')
        .populate('duenioActual')
        .populate('_idPkToken')
        .exec((err,pksolicitudes)=>{
            if (err) {
                return res.status(200).json({
                    ok: false,
                    msj: 'Error cargando pksolicitudes',
                    errors: err
                });
            }

            res.status(200).json({
                ok: true,
                pksolicitudes: pksolicitudes
            })
        })
})

app.get('/:id',(req,res)=>{
    Pksolicitudes.find({duenioActual:req.params.id})
    .populate('solicitante')
    .populate('duenioActual')
    .populate('_idPkToken')
    .sort({fecha:1})
    .exec((err,pksolicitudes)=>{
        if (err) {
            return res.status(200).json({
                ok: false,
                msj: 'Error cargando pksolicitudes',
                errors: err
            });
        }

        res.status(200).json({
            ok:true,
            pksolicitudes:pksolicitudes
        })
    })
})

app.post('/', (req, res) => {

    var body = req.body;
    var resp = false;

    Pksolicitudes.findOne({_idPkToken:body._idPkToken},(err,pkoin)=>{
        if (err) {
            return res.status(500).json({
                ok: false,
                msj: 'Error al crear la solicitud',
                errors: err
            });
        }

        if(pkoin !== null){
            return res.status(200).json({
                ok: false,
                msj: 'Ya existe una solicitud para este pkoin'
            });
        }
        


        pktoken.findOne({_id:body._idPkToken},(err,pkoin)=>{
        pksolicitudData = new Pksolicitudes({
            solicitante: body.solicitante,
            duenioActual: pkoin.duenioActual._id,
            _idPkToken: body._idPkToken,
            fechaCreado: new Date(),
        });


        pksolicitudData.save((err, pksolicitudGuardado) => {
            if (err) {
                return res.status(500).json({
                    ok: false,
                    msj: 'Error al crear la solicitud',
                    errors: err
                });
            }

            Usuario.findById(body.solicitante,(err,usuario)=>{
                if(err){
                    console.log("err")
                }else{
                    notificarSolicitud(pkoin.duenioActual._id,usuario.nombre)
                }
            })
            
            

            res.status(201).json({
                ok: true,
                pksolicitudGuardado,
                msj: "La Solicitud fue enviada correctamente"
            });
        });

    });


    });


    
    
});



function notificarSolicitud(id,nombre) {
    Usuario.findById(id,(err,usuario)=>{
        console.log(usuario)
        var message = new gcm.Message({
            notification: {
                title: "Tienes una solicitud",
                body: `${nombre} esta solicitando 1 pkoin`,
                icon: 'pk_notification_icon'
            },
            data:{
                msg: "Tienes una solicitud nueva"
            }
        });
         
        // Specify which registration IDs to deliver the message to
        var regTokens = [`${usuario.tokenFCM}`];
         
        // Actually send the message
        sender.send(message, { registrationTokens: regTokens }, function (err, response) {
            if(err){
                console.log("error", err)
            }else{
                console.log("ok")
            }
        });
    })
}

app.delete('/:id/:option',(req,res)=>{
    var id = req.params.id;
    var option = req.params.option
    Pksolicitudes.findByIdAndRemove(id)
    .populate('duenioActual')
    .exec((err,pksolicitudes)=>{
        if (err) {
            return res.status(200).json({
                ok: false,
                msj: 'Error cargando pksolicitudes',
                errors: err
            });
        }
        if(option == "rechazar"){
            notificarSolicitudRechazada(pksolicitudes.solicitante,pksolicitudes.duenioActual.nombre)

        }
        res.status(200).json({
            ok:true,
            pksolicitudes:pksolicitudes
        })
    })
})

function notificarSolicitudRechazada(id,nombre) {
    Usuario.findById(id,(err,usuario)=>{
        console.log(usuario)
        var message = new gcm.Message({
            notification: {
                title: "Solicitud cancelada",
                body: `${nombre} rechazo la solicitud`,
                icon: 'pk_notification_icon'
            }
        });
         
        // Specify which registration IDs to deliver the message to
        var regTokens = [`${usuario.tokenFCM}`];
         
        // Actually send the message
        sender.send(message, { registrationTokens: regTokens }, function (err, response) {
            if(err){
                console.log("error", err)
            }else{
                console.log("ok")
            }
        });
    })
}



module.exports = app;