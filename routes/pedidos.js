var express = require('express');
var Usuario = require('../models/usuario');
var Inventario = require('../models/inventario');
var Pedidos= require('../models/pedidos');
var Objectid = require('mongodb').ObjectID;
var Pktoken = require('../models/pktoken')
var app = express();
var moment = require('moment');

var gcm = require("node-gcm");
var sender = new gcm.Sender('AAAAHm1JpCw:APA91bFkZ8juWGdArZ5SKM0KqKwZ777MPa-ANZNAnAXsFwX4MDM5QbiJkLK7lYn81VSf_ADdOfEWfD1gtmGgExui-OCt2bdGbjUrWKdHK9qeYaHnwIjGni-4DmPDt1TvK_1Ibp3xQG1k');

const rol = 'ADMIN_ROLE';
var today = moment().format('DD-MM-YYYY')


app.get('/:status/:fecha', (req, res) => {
    var status = req.params.status;
    var fecha = req.params.fecha;
    Pedidos.find({status, fecha},(err,resp)=>{
        if( err ) {
            return res.status(400).json({
                ok: false,
                msj: 'Error cargando pedido',
                errors: err
            });
        }

        return res.status(200).json({
            ok: true,
            resp
        });
    }); 
});

app.get('/:id', (req, res) => { //Buscar id del usuario para el historial personal
    var id = req.params.id;
    Pedidos.find({ id_usuario:id })  
    .exec((err,pedido)=>{
        if (err) {
            return res.status(200).json({
                ok: false,
                msj: 'Error cargando pedido',
                errors: err
            });
        }

        res.status(200).json({
            ok:true,
            pedido:pedido
        })
    }) 
});

app.get('/producto/bandera/:id', (req, res) => { //Buscar id del producto para el historial admin
    
    var id = req.params.id;
    console.log(id);
    Pedidos.find({id_producto:id},(err,resp)=>{
        if( err ) {
            return res.status(400).json({
                ok: false,
                msj: 'Error cargando pedido',
                errors: err
            });
        }

        return res.status(200).json({
            ok: true,
            resp
        });
    }); 
});

app.get('/', ( req, res, next ) => {
    var desde = req.query.desde || 0;
    desde = Number(desde);

    Pedidos.find({  }, 'nombre email img role google') // se selecciona de las colecciones los campos menos el de la pwd.
    .skip(desde)
    .exec(
        ( err, pedido ) => {
            if( err ) {
                return res.status(200).json({
                    ok: false,
                    msj: 'Error cargando pedido',
                    errors: err
                });
            } 
            Pedidos.count({}, (err, conteo) =>{
                res.status(200).json({
                    ok: true,
                    pedido: pedido,
                    total: conteo
                });
            });
    });
})


app.post('/:id', (req, res) => {
    var id = req.params.id; //id del producto
    var body = req.body;
    var resp = false;
    Inventario.findById( id, (err, resp) =>{
        if (err) {
            return res.status(500).json({
                ok: false,
                msj: 'Error buscar producto',
                err
            });
        }
        
        Usuario.findOne({_id:body.id_usuario},(err,usuario)=>{ // id del usuario
             PedidosData = new Pedidos({
                 id_usuario: body.id_usuario,
                 nom_usuario: usuario.nombre,
                 cantidad:body.cantidad,
                 id_producto:body.id_producto,
                 nom_producto: body.nom_producto,
                 fecha: today,
                 status: body.status,
                 monto:body.monto,

             })

             PedidosData.save((err, pedidoGuardado) =>{
                 if (err){
                     return res.status(500).json({
                         ok: false,
                         msj: "NO se guardo la informacion", 
                         err
                     })
                 } 
                 
                 Usuario.findOne( {role: rol },(err,usuario)=>{
                    if(err){
                        console.log("err")
                    }else{
                        notificarPedido(usuario._id, pedidoGuardado)
                    }

                })
                 
                 res.status(200).json({
                         ok: true,
                         msj: "La Solicitud fue enviada correctamente", 
                         pedidoGuardado,
                    
                 })
             })
 
     });
        
    })

});

function notificarPedido(id, pedido) {
   var pedidos = pedido;
    Usuario.findById(id,(err,user)=>{
        
        var message = new gcm.Message({
            notification: {
                title: "Tienes una solicitud",
                body: `${pedidos} esta solicitando 1 pkoin`,
                icon: 'pk_notification_icon'
            },
            data:{
                msg: "Tienes una solicitud nueva"
            }
        });

        sender.send(message, function (err, response) {
            if(err){
                console.log("error", err)
            }else{
                console.log("ok")
            }
        });
    });
}

// Actualizar status 
app.put('/:id/:status', (req, res) => {
    var id = req.params.id;
    var status = req.params.status;
    const estatus = 'ACEPTADOS' 
    Pedidos.findById(id, (err, pedido) => {
        if( err ) {
            return res.status(500).json({
                ok: false,
                msj: 'Error al buscar pedido',
                errors: err
            });
        }
    
        if (!pedido) {
            return res.status(401).json({
                ok: false,
                msj: 'El producto con el id '+ id + ' no existe',
                errors: { message: 'No existe un producto con ese ID' }
            });
        }

        pedido.status = status;

        if (status === estatus) {
            restaProducto(pedido.id_producto, pedido.cantidad)
        }else{
            console.log('es verificado')
        }

        pedido.save( (err, pedidoGuardado) => {
            if ( err ) {
                return res.status(402).json({
                    ok: false,
                    msj: 'Error al actualizar producto',
                    errors: err
                });
            }

            res.status(200).json({
                ok: true,
                pedido: pedidoGuardado
            });

        })

    });
});

function restaProducto(id, cantidad) {
    var id = id;
    var resto = cantidad;

    Inventario.findById(id ,(err, inventario) => {
        if( err ) {
            return res.status(500).json({
                ok: false,
                msj: 'Error al buscar producto',
                errors: err
            });
        }
    
        if (!inventario) {
            return res.status(400).json({
                ok: false,
                msj: 'El producto con el id '+ id + ' no existe',
                errors: { message: 'No existe un producto con ese ID' }
            });
        }

        inventario.cantidad -= resto;
        inventario.save( (err, inventarioGuardado) => {
            if ( err ) {
                return res.status(400).json({
                    ok: false,
                    msj: 'Error al actualizar producto',
                    errors: err
                });
            }else{
                return  inventarioGuardado
            }
        })
    });
}

// function restaPkoins(id, monto){
//    var  id =id;
//     var monto = monto
//     Pktoken.findById(id ,(err, pktokens) => {
//         if( err ) {
//             return res.status(500).json({
//                 ok: false,
//                 msj: 'Error al buscar producto',
//                 errors: err
//             });
//         }
    
//         Pktoken.count({}, (err, saldo) => {
//             res.status(200).json({
//                 ok: true,
//                 usuarios: pktokens,
//                 total: saldo
//             });
//         });

//         if (saldo >= monto){
//             saldo-=monto;
//             console.log(saldo)
//         }else{

//         }

//         inventario.cantidad -= resto;
//     });
// }

app.delete ( '/:id',(req,res)=>{
    var id = req.params.id;
    Pedidos.findByIdAndRemove(id, (err, pedidoBorrado) => {
        if( err ) {
            return res.status(500).json({
                ok: false,
                msj: 'Error al borrar usuario',
                errors: err
            });
        }

        if( !pedidoBorrado ) {
            return res.status(500).json({
                ok: false,
                msj: 'No existe usuario con ese id',
                errors: { message: 'No existe el pedido con ese id'}
            });
        }

        res.status(200).json({
            ok: true,
            pedido: pedidoBorrado
        });
    })

})

module.exports = app;