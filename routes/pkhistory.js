var express = require('express');
let pktoken = require('../models/pktoken');
var Usuario = require('../models/usuario');
var Objectid = require('mongodb').ObjectID;
var Pktokenhistory = require('../models/pktokenhistory');
var app = express();

app.get('/', (req,res)=>{
    Pktokenhistory.find({})
        .populate('duenioActual','_id nombre email')
        .populate('duenioAnterior','_id nombre email')
        .exec((err,pkhistorys)=>{
            if (err) {
                return res.status(200).json({
                    ok: false,
                    msj: 'Error cargando pkhistorys',
                    errors: err
                });
            }

            res.status(200).json({
                ok: true,
                pkhistorys: pkhistorys
            })
        })
})

app.get('/:id',(req,res)=>{
    Pktokenhistory.find({_idPkToken:req.params.id})
    .sort({fecha:1})
    .populate('duenioActual','_id nombre email')
    .populate('duenioAnterior','_id nombre email')
    .exec((err,pkrecordsbyid)=>{
        if (err) {
            return res.status(200).json({
                ok: false,
                msj: 'Error cargando pkhistorys',
                errors: err
            });
        }

        res.status(200).json({
            ok:true,
            pkrecordsbyid:pkrecordsbyid
        })
    })
})



module.exports = app;