var express = require('express');
const multer = require('multer');
var app = express();
var bcrypt = require('bcryptjs');
var Usuario = require('../models/usuario');
var mdAutenticacion = require('../middlewares/autenticacion');
var gcm = require("node-gcm");
var sender = new gcm.Sender('AAAAHm1JpCw:APA91bFkZ8juWGdArZ5SKM0KqKwZ777MPa-ANZNAnAXsFwX4MDM5QbiJkLK7lYn81VSf_ADdOfEWfD1gtmGgExui-OCt2bdGbjUrWKdHK9qeYaHnwIjGni-4DmPDt1TvK_1Ibp3xQG1k');
// Obtener todos los usuarios.

app.get('/', ( req, res, next ) => {

    var desde = req.query.desde || 0;
    desde = Number(desde);

    Usuario.find({  }, 'nombre email img role google') // se selecciona de las colecciones los campos menos el de la pwd.
    .skip(desde)
    .limit(5)
    .exec(
        ( err, usuarios ) => {
            if( err ) {
                return res.status(200).json({
                    ok: false,
                    msj: 'Error cargando usuarios',
                    errors: err
                });
            } 

        Usuario.count({}, (err, conteo) =>{
            res.status(200).json({
                ok: true,
                usuarios: usuarios,
                total: conteo
            });
        });
    });
});

// Crear nuevo usuario

app.post('/' ,( req, res ) => {
    var body = req.body;
    var usuario = new Usuario({
        nombre: body.nombre,
        email: body.email,
        password: bcrypt.hashSync(body.password, 10),
        img: body.img,
        rol: body.rol
    });

    

    usuario.save( ( err, usuarioGuardado ) => {

        if( err ) {
            return res.status(500).json({
                ok: false,
                msj: 'Error al buscar usuario',
                errors: err
            });
        }

        res.status(201).json({
            ok: true,
            usuario: usuarioGuardado,
            usuarioToken: req.usuario
        });
    });
})

app.post('/tokenfcm/:id',(req,res)=>{
    let body = req.body;
    let id = req.params.id

    Usuario.findByIdAndUpdate(id,{tokenFCM:body.token},(err,usuario)=>{
        console.log(usuario)
    })
})

app.put('/tokenfmc/notification/:id',(req,res)=>{
    
    let id = req.params.id
    let body = req.body
    console.log("llegue",id)

    Usuario.findById(id,(err,usuario)=>{
        var message = new gcm.Message({
            notification: {
                title: "Tienes una solicitud",
                body: `${body.usuario} esta solicitando 1 pkoin`
            }
        });
         
        // Specify which registration IDs to deliver the message to
        var regTokens = [`${usuario.tokenFCM}`];
         
        // Actually send the message
        sender.send(message, { registrationTokens: regTokens }, function (err, response) {
            if(err){
                console.log("error", err)
            }else{
                console.log("ok")
            }
        });
    })
})

//Actualizar usuario

app.put('/:id', (req, res) => {
    var id = req.params.id;
    var body = req.body;
    Usuario.findById(id, (err, usuario) => {
        if( err ) {
            return res.status(500).json({
                ok: false,
                msj: 'Error al buscar usuario',
                errors: err
            });
        }
    
        if (!usuario) {
            return res.status(400).json({
                ok: false,
                msj: 'El usuario con el id '+ id + ' no existe',
                errors: { message: 'No existe un usuario con ese ID' }
            });
        }
        usuario.nombre = body.usuario;
        usuario.email = body.email || usuario.email;
        usuario.role = body.role || usuario.role;
        if(body.password){usuario.password = bcrypt.hashSync(body.password, 10)}
        


        usuario.save( (err, usuarioGuardado) => {
            if ( err ) {
                return res.status(400).json({
                    ok: false,
                    msj: 'Error al actualizar usuario',
                    errors: err
                });
            }

            usuarioGuardado.password = ':)'

            res.status(200).json({
                ok: true,
                usuario: usuarioGuardado
            });

        })

    });
});

// Borrar usuario

app.delete('/:id',( req, res ) => {
    var id = req.params.id;

    Usuario.findByIdAndRemove(id, (err, usuarioBorrado) => {
        if( err ) {
            return res.status(500).json({
                ok: false,
                msj: 'Error al borrar usuario',
                errors: err
            });
        }

        if( !usuarioBorrado ) {
            return res.status(500).json({
                ok: false,
                msj: 'No existe usuario con ese id',
                errors: { message: 'No existe usuario con ese id'}
            });
        }

        res.status(200).json({
            ok: true,
            usuario: usuarioBorrado
        });
    })
});




module.exports = app;