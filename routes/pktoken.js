var express = require('express');
let pktoken = require('../models/pktoken');
var Usuario = require('../models/usuario');
var Objectid = require('mongodb').ObjectID;
var gcm = require("node-gcm");
var sender = new gcm.Sender('AAAAHm1JpCw:APA91bFkZ8juWGdArZ5SKM0KqKwZ777MPa-ANZNAnAXsFwX4MDM5QbiJkLK7lYn81VSf_ADdOfEWfD1gtmGgExui-OCt2bdGbjUrWKdHK9qeYaHnwIjGni-4DmPDt1TvK_1Ibp3xQG1k');


//DAVID PDF PKOINS
const path = require('path');
var fileUrl = require('file-url');
const qrcode = require('qrcode'); 

//let PDF = require('handlebars-pdf');

var pdf = require("pdf-creator-node");

const handlebars = require("handlebars");

var fs = require('fs');

var Pktokenhistory = require('../models/pktokenhistory');
var app = express();

//Obtener Pktokens para un usuario
app.get('/duenio/:owner', (req, res, next) => {
    console.log(req.params.owner)
    let owner = req.params.owner;
    pktoken.find({"duenioActual": {
        "_id": owner,
    }})
        .populate('duenioActual', 'nombre email')
        .exec(
            (err, pktokens) => {
                if (err) {
                    return res.status(200).json({
                        ok: false,
                        msj: 'Error cargando pktokens',
                        errors: err
                    });
                }
                console.log(pktokens)
                pktoken.count({}, (err, conteo) => {
                    res.status(200).json({
                        ok: true,
                        usuarios: pktokens,
                        total: conteo
                    });
                });
            });
});


//Obtener pktokens

app.get('/', (req, res, next) => {
    pktoken.find({})
        .populate('duenioActual', 'nombre email')
        .exec(
            (err, pktokens) => {
                if (err) {
                    return res.status(200).json({
                        ok: false,
                        msj: 'Error cargando pktokens',
                        errors: err
                    });
                }
                pktoken.count({}, (err, conteo) => {
                    res.status(200).json({
                        ok: true,
                        usuarios: pktokens,
                        total: conteo
                    });
                });
            });
});

//Pedir un pktoken

app.get('/:id', (req, res, next) => {
    var id = req.params.id;
    pktoken.findById(id, (err, pktoken) => {
        if (err) {
            return res.status(500).json({
                ok: false,
                msj: 'Error al buscar pktoken',
                errors: err
            });
        }

        if (pktoken) {
            return res.status(400).json({
                ok: false,
                msj: 'El pktoken con el id ' + id + ' existe',
                errors: { message: 'genere otro pktoken' }
            });
        }

        return res.status(200).json({
            ok: true,
        });
    });
});

//Crear un pktoken

app.post('/', (req, res) => {

    var body = req.body;
    pktokenData = new pktoken({
        _id: JSON.parse(body._id)._id,
        monto: body.monto,
        fechaCreado: new Date(),
        duenioActual: body.duenioActual,
        estatus: false
    });

    pktokenData.save((err, pktokenGuardado) => {
        if (err) {
            return res.status(500).json({
                ok: false,
                msj: 'Error al buscar el pktoken',
                errors: err
            });
        }

        res.status(201).json({
            ok: true,
            pktokenGuardado
        });
    });
});

//Crear muchos pktokens
app.get('/:cantidad/:monto/:numerolote/:id', (req, res) => {
    var cantidad = req.params.cantidad;
    var numerolote = req.params.numerolote;
    var monto = req.params.monto;
    var id = req.params.id;

    var numeroslote= [];

    var qrs = [];

    function tokenNumber() {
        return 'xxxxxxxxxxxxxxxxxxxxxxxx'.replace(/[xy]/g, function(c) {
          var r = Math.random() * 16 | 0, v = c == 'x' ? r : (r & 0x3 | 0x8);      
          return v.toString(16);
        });
    }

    async function creacion(cantidad){
            return await new Promise((resolve)=>{
                var contador = 0;
                for(let i = 0; i<cantidad; i++){
                    pktokenData = new pktoken({
                        _id: tokenNumber(),
                        monto: monto,
                        fechaCreado: new Date(),
                        duenioActual: id,
                        estatus: false
                    });
                    console.log(pktokenData._id)
                    
                    let idpkoin= JSON.stringify(pktokenData._id+" ");
                    let pktokenID = idpkoin.substring(19,24);

                    let tokenumber = numerolote;
                    let tokenid = tokenumber.substring(11,20);
                    let tokenid1 = tokenumber.substring(0, 6);
                    let tokencomplete = tokenid1 + pktokenID + tokenid;

                    console.log(tokenid)
                    console.log(tokenid1)
                    console.log(tokenumber)

                    numeroslote.push(tokencomplete);
                
                    pktokenData.save((err, pktokenGuardado) => {
                        if (err) {
                            console.log(err);
                        }
                        
                        console.log(pktokenGuardado._id);
        
                        qrs.push(pktokenGuardado._id);
                        contador++;
                        if(contador.toString() === cantidad.toString()){
                            resolve(qrs);
                        }
                        
                    });
                    
                }
            });
            
    } 

    var x = creacion(cantidad).then((qrs)=> {
        var c = () => {
            creacionqr(qrs).then((qrss)=> 
                {
                    var array= [];
                    var urlbillete = path.resolve( __dirname, '../assets/billete.jpg' ); 
                    var billete = fileUrl(urlbillete);
                    var html = fs.readFileSync(path.resolve( __dirname, '../templates/pdf.html' ), 'utf8');
                    var template = handlebars.compile(html);

                    for(let i = 0; i<cantidad; i++){
                        array.push({
                            url: billete,
                            qr: qrss[i],
                            numerolote: numeroslote[i]
                        })
                        console.log(array[i])
                        console.log('---------------------------------'+i+'----------------------------------')
                    }

                    var salida = template({
                        billeteurl: billete,
                        cantidad: array
                    })

                    var document = {
                        html: salida,
                        data: {
                            url: billete
                        },
                        path: "./lotes/lote-"+numerolote+".pdf"
                    };
                
                    var options = {
                        format: "Letter",
                        orientation: "portrait",
                        border: {
                            "top": "0mm",            // default is 0, units: mm, cm, in, px
                            "right": "10mm",
                            "bottom": "20mm",
                            "left": "0mm"
                        }
                    };
                
                    pdf.create(document, options)
                    .then(res => {
                        confirmar();
                    })
                    .catch(error => {   
                        console.error(error)
                    });




                }
            );
        };

        c()
    });
    
    async function creacionqr(qrs){
        return await new Promise((resolve)=>{
            var qrss= []
            for(let i = 0; i<cantidad; i++){
                qrcode.toDataURL(qrs[i].toString()).then(qr => {
                    console.log("QR GENERADO")
                    qrss.push(qr);
                    if(i === cantidad-1){
                        resolve(qrss);
                    }
                });
            }
        });

            
    }

    function confirmar(){
        var urlpdf = path.resolve( __dirname, '../lotes/lote-'+numerolote+'.pdf' ); 
        res.sendFile(urlpdf)
    }
});


//Borrar pktoken

app.delete('/:id' ,( req, res ) => {
    var id = req.params.id;

    pktoken.findByIdAndRemove(id, (err, pktokenBorrado) => {
        if( err ) {
            return res.status(500).json({
                ok: false,
                msj: 'Error al borrar medico',
                errors: err
            });
        }

        if( !pktokenBorrado ) {
            return res.status(500).json({
                ok: false,
                msj: 'No existe medico con ese id',
                errors: { message: 'No existe medico con ese id'}
            });
        }

        res.status(200).json({
            ok: true,
            usuario: pktokenBorrado
        });
    })
});



//Actualizar dueño pktoken

app.put('/:id', (req, res) => {
    var id = req.params.id;
    
    Promise.all([
            actualizaPkToken(id, req.body.nuevoDuenio)
        ])
        .then(respuestas => {
            console.log(respuestas);
            res.status(200).json({
                ok: true,
                // pktoken: respuestas[0],
                // usuario: respuestas[1]
            });
        });
})

let actualizaPkToken = (id, nuevoDuenio) => {
    return new Promise((resolve, reject) => {
        pktoken.findById(id)
            .populate('duenioActual')
            .exec((err,pktokenF)=>{
                console.log(nuevoDuenio,'nuevo')
                
                registrarTraspaso(id,nuevoDuenio).then((msg)=>{
                    pktoken.findByIdAndUpdate(id, { $set: { duenioActual: Objectid(nuevoDuenio), estatus: true } },(err,pktokenUp)=>{
                        console.log(pktokenUp)
                        notificarSolicitudAceptada(nuevoDuenio,pktokenF.duenioActual.nombre)
                    })
                    
                })

            })
        
        
    });
}

let registrarTraspaso = (id,newOwner) => {
    return new Promise((resolve,reject)=>{
        console.log(id)
        pktoken.findOne({_id:id},(err,pkoin)=>{
           
            pkhistory = new Pktokenhistory({
                duenioAnterior: pkoin.duenioActual,
                _idPkToken: id,
                duenioActual: newOwner
            })
            pkhistory.save().then((result)=>{
                resolve(result)
            })
        })
    })
}



function notificarSolicitudAceptada(id,nombre) {
    Usuario.findById(id,(err,usuario)=>{
        
        var message = new gcm.Message({
            notification: {
                title: "Solicitud aceptada",
                body: `${nombre} Acepto la solicitud por 1 pkcoin`,
                icon: 'pk_notification_icon'
            }
        });
         
        // Specify which registration IDs to deliver the message to
        var regTokens = [`${usuario.tokenFCM}`];
         
        // Actually send the message
        sender.send(message, { registrationTokens: regTokens }, function (err, response) {
            if(err){
                console.log("error", err)
            }else{
                console.log("ok")
            }
        });
    })
}




module.exports = app;