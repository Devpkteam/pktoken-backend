var mongoose = require("mongoose");
var Schema = mongoose.Schema;

var pktokenhistorySchema = Schema({
    _idTransaction: { type: Schema.Types.ObjectId },
    duenioActual: { type: Schema.Types.ObjectId, ref: 'Usuario' },
    duenioAnterior: { type: Schema.Types.ObjectId, ref: 'Usuario' },
    _idPkToken: {type: Schema.Types.ObjectId, ref: 'Pktoken'},
    fecha: { type: Date , default: Date.now()},
});

module.exports = mongoose.model('pktokenhistory', pktokenhistorySchema);