var mongoose =	require('mongoose');
var Schema =	mongoose.Schema;
var imageSchema =	new Schema({
    filename: {	type: String},
    originalName: {	type: String },
    desc: {	type: String },
    created: {	type: Date, default: Date.now()	},
    url: {type:String}
});

module.exports = mongoose.model('Image',	imageSchema);