var mongoose = require('mongoose');
var uniqueValidator = require('mongoose-unique-validator');

var Schema = mongoose.Schema;


var rolesValidos = {
    values: ['ADMIN_ROLE', 'USER_ROLE'],
    message: '{VALUE} no es un rol permitido'
};
    

var inventarioSchema = new Schema({

    nombre: { type: String, required: [true, 'El nombre es necesario'] },
    cantidad: { type: Number, required: [true, 'La cantidad es necesario'] },
    costo: { type: Number, required: [true, 'El costo es necesario'] },
    costo_inversion: { type: Number, required: [true, 'El costo de inversion es necesario'] },
    tokenFCM: { type: String }
});
//USER_ROLE
inventarioSchema.plugin(uniqueValidator, { message: '{PATH} debe de ser único' });

module.exports = mongoose.model('Inventario', inventarioSchema);