var mongoose = require("mongoose");
var uniqueValidator = require('mongoose-unique-validator');
var Schema = mongoose.Schema;

var pedidoSchema = Schema({
    id_usuario: { type: Schema.Types.ObjectId, ref: 'Usuario' },
    nom_usuario: {type: String, required: [true, 'El nombre es necesario'] },
    id_producto: {type: String, required: [true, 'La id es necesario'] },
    nom_producto: {type: String, required: [true, 'El nombre del producto es necesario']},
    monto: {type: Number, required: [true, 'El monto es necesario']},
    cantidad: {type: Number, required: [true, 'La cantidad es necesario'] } ,
    fecha: { type: String },
    status: { type: String, required: [true, 'El status es necesario'] },
});

module.exports = mongoose.model('pedidos', pedidoSchema);