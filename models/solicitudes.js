var mongoose = require("mongoose");
var Schema = mongoose.Schema;

var solicitudesSchema = Schema({
    solicitante: { type: Schema.Types.ObjectId, ref: 'Usuario' },
    duenioActual: { type: Schema.Types.ObjectId, ref: 'Usuario' },
    _idPkToken: {type: Schema.Types.ObjectId, ref: 'pktoken'},
    fecha: { type: Date , default: Date.now()},
});

module.exports = mongoose.model('solicitudes', solicitudesSchema);