var mongoose = require('mongoose');
var Schema = mongoose.Schema;
var pktokenSchema = new Schema({
    _id: { type: Schema.Types.ObjectId },
    monto: { type: String, required: false },
    fechaCreado: { type: Date },
    duenioActual: { type: Schema.Types.ObjectId, ref: 'Usuario' },
    estatus: { type: Boolean },
    deuda: { type: String, required: false },
});

module.exports = mongoose.model('pktoken', pktokenSchema);