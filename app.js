// Requires

var express = require('express');
var mongoose = require('mongoose');
var bodyParser = require('body-parser');
const cors = require("cors");

var socketio = require('socket.io');

//Inicializar variables

var app = express();

// CORS
app.use(cors({ origin: "*" }));
app.use(function(req, res, next) {
    res.header("Access-Control-Allow-Origin", "*"); // update to match the domain you will make the request from
    res.header("Access-Control-Allow-Headers", "Origin, X-Requested-With, Content-Type, Accept");
    res.header("Access-Control-Allow-Methods", "POST, GET, PUT, DELETE, OPTIONS");
    res.header('Access-Control-Allow-Credentials', true);
    next();
});


// bodyParser 

app.use(bodyParser.urlencoded({ extended: false }))
app.use(bodyParser.json());

// Importar rutas

var appRoutes = require('./routes/app');
var usuarioRoutes = require('./routes/usuario');
var inventarioRoutes = require('./routes/inventario');
var loginRoutes = require('./routes/login'); 
var hospitalRoutes = require('./routes/hospital');
var medicoRoutes = require('./routes/medico');
var busquedaRoutes = require('./routes/busqueda');
var uploadRoutes = require('./routes/upload');
var imgenesRoutes = require('./routes/imagenes');
var pkTokenRoutes = require('./routes/pktoken');
var pedidos = require('./routes/pedidos');
var pkHistory = require('./routes/pkhistory')
var pkSolicitudes = require('./routes/pksolicitudes')

// Conexion a la BD
mongoose.connect('mongodb://localhost:27017/pkTokenDB', { useNewUrlParser: true ,useFindAndModify: false }, (err, res) => {
    if (err) throw err;
    console.log('Base de datos: \x1b[32m%s\x1b[0m', 'online');
});

// var serveIndex = require('serve-index');
// app.use(express.static(__dirname + '/'))
// app.use('/uploads', serveIndex(__dirname + '/uploads'));


// Rutas

app.use('/pktoken', pkTokenRoutes);
app.use('/usuario', usuarioRoutes);
app.use('/inventario', inventarioRoutes);
app.use('/login', loginRoutes);
app.use('/hospital', hospitalRoutes);
app.use('/medico', medicoRoutes);
app.use('/busqueda', busquedaRoutes);
app.use('/upload', uploadRoutes);
app.use('/img', imgenesRoutes);
app.use('/', appRoutes);
app.use('/pkhistory',pkHistory);
app.use('/solicitudes',pkSolicitudes);
app.use('/pedidos', pedidos);




// Escuchar peticiones

var server = app.listen(3000, () => {
    console.log('Express server puerto 3000: \x1b[32m%s\x1b[0m', 'online');
});


// WEB SOCKET

var messages = [{
	author: "Carlos",
    text: "Hola! que tal?"
},{
	author: "Pepe",
    text: "Muy bien! y tu??"
},{
	author: "Paco",
    text: "Genial!"
}];

var io = socketio.listen(server);

io.on('connection', function(socket) {
	console.log('Un cliente se ha conectado');
    socket.emit('messages', messages);
});

